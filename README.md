## 只需要三步即可使用Python操作数据库
```python
第一步：安装模块pymysql
1. pip install pymysql
第二步：创建实例
zmysql = ZMysql(host='主机名',port = 3306,database="数据库",user='数据库用户名',password='数据库密码',charset='utf8') 
第三步：执行方法
# 插入语句：返回插入后的记录主键id(如返回1)
# 查询语句，返回的是元组包含对象
# 如果是是修改直接修改即可
result = zmysql .execute_sql("您的SQL语句")
```

例如：
```python
my = ZMysql(database="notes",user='root',password='dellzhu')
########S 插入########
result = my.execute_sql("INSERT INTO category (name,status)VALUES('技术',1)")
print(result)
# 返回主键id:3
########E 插入########

########S 删除数据########
result = my.execute_sql("DELETE FROM category WHERE id=3")
print(result)
# 返回：0 如果不报异常表示删除成功返回0 否则就是删除失败
########E 删除数据########


########S 修改数据########
result = my.execute_sql("UPDATE note_category SET name='新技术' WHERE id=4")
print(result)
# 返回：0 如果修改成功或者改记录不存在都返回0 报异常表示失败
########E 修改数据########


########S 查询########
result = my.execute_sql("SELECT * FROM USER")
print(result)
输出结果((1, '张三', 'xxxx', "二品"),(2, '里斯', '18', "一品"))
########E 查询########
```
